/*
Assume each elem has the following structure: {val int,link: memory address}
 */

function arrayToList(arr)
{
	ll = {val: arr[0], link: null};

	ll_p = ll;

	var i = 1;

	for ( ; i < arr.length; i++ )
	{
		ll.link = {val: arr[i], link: null};

		ll = ll.link;

	}

	return ll_p;

}

function listToArray(list)
{
	arr = [];

	list_p = list;

	for ( ; list_p  != null; list_p = list_p.link )
	{
		arr.push(list_p.val);
	}

	return arr;

}

function prepend(list,elem)
{
	front = { val: elem, link: list};

	return front;
}

function nth_iter(list,pos)
{
	var i = 0;

	list_p = list;

	for ( ; (list_p != null) && (i < pos); list_p = list_p.link, i++ )
		;
	
	if ( list_p == null )
	{
		return undefined;
	}

	return list_p.val;
}

function nth_recursion(list,pos)
{
	if ( list == null )
	{
		return undefined;
	}

	if ( pos == 0 )
	{
		return list.val;
	}

	return nth_recursion(list.link,pos-1);

}


sll = arrayToList([1,2,3,4,5]);

varr = listToArray(sll);

console.log(varr);

console.log(nth_recursion(sll,2));

console.log(nth_iter(sll,2));

