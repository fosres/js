function reverseArrayInPlace(list)
{
	var i = 0, j = list.length - 1, temp = 0;

	for ( ; i < j; i++,j-- )
	{
		temp = list[i];

		list[i] = list[j];

		list[j] = temp;
	}
}

arr = [1,2,3];

reverseArrayInPlace(arr);

console.log(arr);
