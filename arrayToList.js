/*
Assume each elem has the following structure: {val int,link: memory address}
 */

function arrayToList(arr)
{
	ll = {val: arr[0], link: null};

	ll_p = ll;

	var i = 1;

	for ( ; i < arr.length; i++ )
	{
		ll.link = {val: arr[i], link: null};

		ll = ll.link;

	}

	return ll_p;

}


sll = arrayToList([1,2,3]);

console.log(sll);

sll_p = sll;

for ( ; sll_p.link != null; sll_p = sll_p.link )
{
	console.log(sll_p.val);
}

console.log(sll_p.val);


