function reverse(list)
{
	var i = list.length - 1;

	var res = [];

	for ( ; i >=  0; i-- )
	{
		res.push(list[i]);
	}

	return res;
}

arr = ['t', 'h', 'r', 'e', 'e'];

console.log(reverse(arr));
