function range(start,end,step)
{

	if ( (step > 0) && (start > end) )
	{
		console.log("Error: step > 0 yet (start > end)");

		return [];
	}

	else if ( (step < 0) && (start < end) )
	{
		console.log("Error: step < 0 yet (start < end)");

		return [];
	}
	
	var arr = []

	var i = start;

	if ( step > 0 )
	{

		for ( ; i <= end; i += step )
		{
			arr.push(i);
		}
	}

	else if ( step < 0 )
	{

		for ( ; i >= end; i += step )
		{
			arr.push(i);
		}
	}

	i = 0;

	return arr;

}

function sum(list)
{
	var i = 0, sum = 0;

	for ( ; i < list.length; i++ )
	{
		sum += list[i];	
	}

	return sum;
}

console.log(range(10,5,-1));

//console.log(sum(range(1,100)));

