/*
Assume each elem has the following structure: {val int,link: memory address}
 */

function arrayToList(arr)
{
	ll = {val: arr[0], link: null};

	ll_p = ll;

	var i = 1;

	for ( ; i < arr.length; i++ )
	{
		ll.link = {val: arr[i], link: null};

		ll = ll.link;

	}

	return ll_p;

}

function listToArray(list)
{
	arr = [];

	list_p = list;

	for ( ; list_p  != null; list_p = list_p.link )
	{
		arr.push(list_p.val);
	}

	return arr;

}

function prepend(list,elem)
{
	front = { val: elem, link: list};

	return front;
}


sll = arrayToList([1,2,3,4,5]);

sll_new = prepend(sll,0);

varr = listToArray(sll_new);

console.log(varr);

console.log(sll_new);
