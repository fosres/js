/*
Assume each elem has the following structure: {val int,link: memory address}
 */

function arrayToList(arr)
{
	ll = {val: arr[0], link: null};

	ll_p = ll;

	var i = 1;

	for ( ; i < arr.length; i++ )
	{
		ll.link = {val: arr[i], link: null};

		ll = ll.link;

	}

	return ll_p;

}

function listToArray(list)
{
	arr = [];

	list_p = list;

	for ( ; list_p  != null; list_p = list_p.link )
	{
		arr.push(list_p.val);
	}

	return arr;

}


sll = arrayToList([1,2,3]);

varr = listToArray(sll);

console.log(varr);
